# TP5 : TCP, UDP et services réseau

## I. First steps

🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

  
  

🌞 Demandez l'avis à votre OS

## II. Setup Virtuel

### 1. SSH

🌞 Examinez le trafic dans Wireshark


🌞 Demandez aux OS

    SSDPSRV
    [svchost.exe]
    UDP    10.5.1.10:137          *:*
    Impossible d’obtenir les informations de propriétaire
    UDP    10.5.1.10:138          *:*
    Impossible d’obtenir les informations de propriétaire
    UDP    10.5.1.10:1900         *:*
    SSDPSRV
    [svchost.exe]
    UDP    10.5.1.10:5353         *:*
    [nvcontainer.exe]
    UDP    10.5.1.10:5353         *:*
    [mDNSResponder.exe]
    UDP    10.5.1.10:57610        *:*
#
    [nono@node1 ~]$ ss -antp
    State        Recv-Q       Send-Q             Local Address:Port             Peer Address:Port       Process
    LISTEN       0            128                      0.0.0.0:22                    0.0.0.0:*
    ESTAB        0            52                     10.5.1.11:22                  10.5.1.10:54919
    LISTEN       0            128                         [::]:22                       [::]:*

### 2. Routage

🌞 Prouvez que

    [nono@node1 ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=110 time=72.3 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=110 time=97.5 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=110 time=67.1 ms
    ^C
    --- 8.8.8.8 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2004ms
    rtt min/avg/max/mdev = 67.064/78.967/97.495/13.277 ms
    [nono@node1 ~]$ ping google.com
    PING google.com (172.217.16.142) 56(84) bytes of data.
    64 bytes from fra15s46-in-f14.1e100.net (172.217.16.142): icmp_seq=1 ttl=110 time=86.9 ms
    64 bytes from zrh04s06-in-f142.1e100.net (172.217.16.142): icmp_seq=2 ttl=110 time=54.7 ms
    64 bytes from zrh04s06-in-f142.1e100.net (172.217.16.142): icmp_seq=3 ttl=110 time=81.6 ms
    ^C64 bytes from 172.217.16.142: icmp_seq=4 ttl=110 time=88.0 ms

    --- google.com ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 15528ms
    rtt min/avg/max/mdev = 54.661/77.784/87.982/13.566 ms

### 3. Serveur Web

🌞 Installez le paquet nginx

    [nono@localhost ~]$ sudo dnf install nginx

🌞 Créer le site web

    [nono@localhost ~]$ sudo nano /var/www/site_web_nul/index.html

🌞 Donner les bonnes permissions

    [nono@localhost ~]$ sudo chown -R nginx:nginx /var/www/site_web_nul

🌞 Créer un fichier de configuration NGINX pour notre site web

    [nono@localhost ~]$ sudo nano /etc/nginx/conf.d/site_web_nul.conf

🌞 Démarrer le serveur web !

    [nono@localhost ~]$ sudo nano /etc/nginx/conf.d/site_web_nul.conf
    [nono@localhost ~]$ sudo systemctl start nginx
    [nono@localhost ~]$ systemctl status nginx
    ● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Fri 2023-11-10 16:19:04 CET; 14s ago

🌞 Ouvrir le port firewall

    [nono@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    [nono@localhost ~]$ sudo firewall-cmd --reload
    success

🌞 Visitez le serveur web !

    [nono@node1 ~]$ curl 10.5.1.12
    <h1>MOEW</h1>

🌞 Visualiser le port en écoute

    [nono@localhost ~]$ ss -antp
    State          Recv-Q      Send-Q           Local Address:Port            Peer Address:Port       Process
    LISTEN         0           128                    0.0.0.0:22                   0.0.0.0:*
    LISTEN         0           511                    0.0.0.0:80                   0.0.0.0:*
    TIME-WAIT      0           0                    10.5.1.12:80                 10.5.1.10:55323
    TIME-WAIT      0           0                    10.5.1.12:80                 10.5.1.10:55322
    ESTAB          0           52                   10.5.1.12:22                 10.5.1.10:55130
    LISTEN         0           128                       [::]:22                      [::]:*
    LISTEN         0           511                       [::]:80                      [::]:*

🌞 Analyse trafic

